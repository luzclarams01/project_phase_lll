from tkinter import *
import tkinter as tk
from tkinter import messagebox
from PIL import ImageTk
import requests
import pickle
import cognitive_face as CF
from PIL import Image, ImageDraw, ImageFont

subscription_key = None
SUBSCRIPTION_KEY = 'cdafd7788a4f4e178bc85046917a1b79'
BASE_URL = 'https://classes.cognitiveservices.azure.com/face/v1.0/'
CF.BaseUrl.set(BASE_URL)
CF.Key.set(SUBSCRIPTION_KEY)

def create_person(name, image_path, group_id):
    """This function is to createa person, and add it to a 
    group depending on the characteristics of the person and the group."""
    response = CF.person.create(group_id, name)
    person_id = response['personId']
    CF.person.add_face(image_path, group_id, person_id)
    CF.person_group.train(group_id)
    response = CF.person_group.get_status(group_id)
    status = response['status']
    print(status)
    return person_id

def emotions(picture,bools):
    """It is in charge of analyzing the emotions through the Microsoft Azure system provided by the person's image."""
    image_path = picture
    image_data = open(image_path, "rb").read()
    headers = {'Ocp-Apim-Subscription-Key': SUBSCRIPTION_KEY,
    'Content-Type': 'application/octet-stream'}
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': 'age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise',
    }
    response = requests.post(
                             BASE_URL + "detect/", headers=headers, params=params, data=image_data)
    analysis = response.json()
    dic = analysis[0]
    fa = dic['faceAttributes']
    if bools == True:
        gender = fa['gender']
        age = fa['age']
        return gender, age
    else:
        print(fa['emotion'])

"""Classess and child classes."""
class Person():
    """It is in charge of providing the data to the people that 
    are going to be created, as personal data and extras."""
    def __init__(self, id_1, personId, name, gender, age, image_path):
        self.id_1 = id_1
        self. personId = personId
        self.name = name
        self.gender = gender
        self.age = age
        self.image_path = image_path

class Family(Person):
    """This function belongs to the family unit, since it 
    assigns a family with its respective data."""
    def __init__(self, id_1, personId, name, age, gender, image_path, pro_1, pro_2):
        super().__init__(id_1, personId, name, gender, age, image_path)
        self.pro_1 = pro_1
        self.pro_2 = pro_2

class Friends(Person):
    """This function is of the dependence friends, 
    since it assigns a family with its respective data."""
    def __init__(self, id_1, personId, name, age, gender, image_path, pro_1, pro_2):
        super().__init__(id_1, personId, name, gender, age, image_path)
        self.pro_1 = pro_1
        self.pro_2 = pro_2

class Famous(Person):
    """This function is of the famous dependency, 
    since it assigns a family with its respective data."""
    def __init__(self, id_1, personId, name, age, gender, image_path, pro_1, pro_2):
        super().__init__(id_1, personId, name, gender, age, image_path)
        self.pro_1 = pro_1
        self.pro_2 = pro_2

def binary_files(person):
    """Storage data in binary file, where people's data is located."""
    with open("person.bin", "ab") as f:
        pickle.dump(person, f, pickle.HIGHEST_PROTOCOL)

def read_file(group_id):
    """Read the binary data to request 
    registration of data on groups and individuals."""
    f = open("person.bin", "rb")
    f.seek(0)
    flag = 0
    list1=[]
    while flag ==0:
        try:
            e = pickle.load(f)
            list1.append([e.id_1, e.personId, e.name, e.age, e.gender, e.image_path, e.pro_1, e.pro_2])
        except:
            print("")
            flag = 1
    f.close
    return list1

def print_people(group_id):  
    """Calls the person in the file list 
    from the group where the person's ID is located"""
    number_group=CF.person.lists(group_id)
    o=read_file(group_id)
    list1=[]
    for i in number_group:
        person_id=i['personId']
        for f in o:
            if f[1]==person_id:
                list1.append(f)
    return list1

def partition_list(l):
    """Order numbers from lowest to highest."""
    minors = list()
    greater = list()
    pivote = l[-1]
    for x in l[:-1]:
        if x < pivote:
            minors.append(x)
        else:
            greater.append(x)
    return([minors, pivote, greater])

def quick_Sort(list0):
    """The sorting function is created by quickSort, which return a sorted list."""
    if len(list0)>0:
        list0 = partition_list(list0)
        cont = 0
        while cont < len(list0):
            e = list0[cont]
            if type(e) == list:
                if len(e) == 0:
                    del list0[cont]
                elif len(e) == 1:
                    list0[cont] = e[0]
                else:
                    list0 = list0[:cont]+partition_list(e) + list0[cont+1:]
            else:
                cont = cont + 1
    return (list0)

def ages(group_id):
    """Shows the ages of the people in the scanned image."""
    list1=print_people(group_id)
    ages = []
    """Browses through a list to find the age 
    in the attributes' list to add them to a new list."""
    for face in list1:
        age = face[3]
        if age >= 0:
            ages.append(age)
    return ages

def ages_order(group_id):
    """Age-ordering function """
    ages1=ages(group_id)
    ages2=quick_Sort(ages1)
    return ages2

def names_sorted(group_id):
    """It is in charge of ordering the names of the people, who are located in the groups."""
    list1=print_people(group_id)
    names = []
    """Browses through a list to find the names list to add them to a new list."""
    for n in list1:
        names.append(n[2])   
    list_names=[]
    a='A'
    cont=0
    for i in names:
        if i>a:
            list_names.append(i)
        elif i<list_names[0]:
            list_names.insert(0,i)
        else:
            for e in names:
                if i<e and i>names[cont+1]:
                    list_names.insert((cont+1),i)
                    break
                cont+=1
        a=list_names[-1]
    return(list_names)

class Window(tk.Frame):
    
    def __init__(self, master=None):
        """Parameters you want to send through the constructor class."""
        Frame.__init__(self, master) 
        self.master = master
        """Show menu."""
        self.start_menu() 
        
    def start_menu(self):
        """Main function of the execution for the menu."""
        self.master.title("PLATFORM")
        self.pack(fill=BOTH, expand=1)
        """Create menu instance."""
        menu = Menu(self.master)
        self.master.config(menu=menu)  
        """Create the File object."""
        archive = Menu(menu)
        """Add commands to menu option
        Exit option."""
        archive.add_command(label="Exit", command=self.exit)
        menu.add_cascade(label="Archive", menu=archive)
        """Create the window's object."""
        windows = Menu(menu)
        windows.add_command(label="Create new group", command=self.create_group_window)
        windows.add_command(label="Create a person in a group", command=self.create_person_group)
        windows.add_command(label="Facial recognition", command=self.recognize)
        windows.add_command(label="Print people information sorted by age or name", command=self.people_information_sorted)
        menu.add_cascade(label="Requests.", menu=windows)
     
    def exit(self, *argument):
        """Function that executes the output action of the menu."""
        exit()
    
    def close_window(self, *argument):
        """Closing function of the executable graphic interface."""
        print("Thank you for using our platform.")
        
    def create_person_group(self):
        """Function of creation of people in certain groups with attached graphic interface."""
        global create_person_group_1
        create_person_group_1 = tk.Toplevel()
        create_person_group_1.title("Create a person in a group")
        create_person_group_1.geometry("800x200")
        create_person_group_1.config(bg="light green")

        create_person_group_1.lb_inst = tk.Label(create_person_group_1, text="Options for the space named the group ID:")
        create_person_group_1.lb_inst.grid(column=0, row=0, padx=(20,10)),create_person_group_1.lb_inst.config(bg="light green")

        create_person_group_1.lb_inst1 = tk.Label(create_person_group_1, text="1. Family")
        create_person_group_1.lb_inst2 = tk.Label(create_person_group_1, text="2. Friends")
        create_person_group_1.lb_inst3 = tk.Label(create_person_group_1, text="3. Famous")
        create_person_group_1.lb_inst1.grid(column=0, row=1, padx=(20,10)),create_person_group_1.lb_inst1.config(bg="light green")
        create_person_group_1.lb_inst2.grid(column=0, row=2, padx=(20,10)),create_person_group_1.lb_inst2.config(bg="light green")
        create_person_group_1.lb_inst3.grid(column=0, row=3, padx=(20,10)),create_person_group_1.lb_inst3.config(bg="light green")

        create_person_group_1.lb_group_id = tk.Label(create_person_group_1, text="Group ID: ")
        create_person_group_1.sv_group_id = tk.StringVar()
        create_person_group_1.tb_group_id = tk.Entry(create_person_group_1, textvariable = create_person_group_1.sv_group_id, width=40)

        create_person_group_1.b_exit = tk.Button(create_person_group_1, text = "Exit" , 
                                     command = lambda:self.close_window(create_person_group_1.destroy()))

        create_person_group_1.b_send = tk.Button(create_person_group_1, text = "Send",
                                     command = lambda: self.info_person_group1(
                                     create_person_group_1.sv_group_id.get(),
                                     create_person_group_1.sv_group_id.set("")      
                                       ))
      
        create_person_group_1.lb_group_id.grid(column=1, row=2, padx=(20,10)),create_person_group_1.lb_group_id.config(bg="light green")
        create_person_group_1.tb_group_id.grid(column=2, row=2,  pady=5, columnspan=2, padx=(20,10))
        create_person_group_1.b_exit.grid(column=3, row=7, pady=15)
        create_person_group_1.b_send.grid(column=2, row=7, pady=15)

    def info_person_group1(self, *argument):
        """Function that generates the corresponding information depending on the information of the suggested group."""
        create_person_group_1.iconify()
        group_id = argument[0]
        group_id = int(group_id)
        if group_id ==1:
            text1 = "Enter relationship between you and the person: "
            text2 = "Enter the person's nationality: "
        elif group_id ==2:
            text1 = "When did your friendship begin? Enter the date: "
            text2 = "Enter the person's nationality: "
        elif group_id ==3:
            text1 = "Enter the person's profession:  "
            text2 = "Enter the description you perform: "
        info_person_group = tk.Toplevel()
        info_person_group.config(bg="aquamarine")
        info_person_group.title("Enter the person's data:")
        info_person_group.lb_Id = tk.Label(info_person_group, text="ID: ")
        info_person_group.lb_name = tk.Label(info_person_group, text="Name: ")
        info_person_group.lb_image_path = tk.Label(info_person_group, text="Image's path: ")
        info_person_group.lb_pro_1 = tk.Label(info_person_group, text=text1)
        info_person_group.lb_pro_2 = tk.Label(info_person_group, text=text2)
        info_person_group.sv_Id = tk.StringVar()
        info_person_group.tb_Id = tk.Entry(info_person_group, textvariable = info_person_group.sv_Id, width=40)
        info_person_group.sv_name = tk.StringVar()
        info_person_group.tb_name = tk.Entry(info_person_group, textvariable = info_person_group.sv_name, width=40)
        info_person_group.sv_image_path = tk.StringVar()
        info_person_group.tb_image_path = tk.Entry(info_person_group, textvariable = info_person_group.sv_image_path, width=40)
        info_person_group.sv_pro_1 = tk.StringVar()
        info_person_group.tb_pro_1 = tk.Entry(info_person_group, textvariable = info_person_group.sv_pro_1, width=40)
        info_person_group.sv_pro_2 = tk.StringVar()
        info_person_group.tb_pro_2 = tk.Entry(info_person_group, textvariable = info_person_group.sv_pro_2, width=40)
        info_person_group.b_exit = tk.Button(info_person_group, text = "Exit",
                                     command = lambda: self.close_window(info_person_group.withdraw()))
        info_person_group.b_save = tk.Button(info_person_group, text = "Send",
                                     command = lambda: self.create_person_in_group(
                                     group_id,
                                     info_person_group.sv_Id.get(),
                                     info_person_group.sv_name.get(),
                                     info_person_group.sv_image_path.get(),
                                     info_person_group.sv_pro_1.get(),
                                     info_person_group.sv_pro_2.get(),
                                     info_person_group.sv_Id.set(""),
                                     info_person_group.sv_name.set(""),
                                     info_person_group.sv_image_path.set(""),
                                     info_person_group.sv_pro_1.set(""),
                                     info_person_group.sv_pro_2.set("")      
                                    ))                         
        info_person_group.lb_Id.grid(column=0, row=0, padx=(20,10)),info_person_group.lb_Id.config(bg="aquamarine")
        info_person_group.lb_name.grid(column=0, row=1, padx=(20,10)),info_person_group.lb_name.config(bg="aquamarine")
        info_person_group.lb_image_path.grid(column=0, row=2, padx=(20,10)),info_person_group.lb_image_path.config(bg="aquamarine")
        info_person_group.lb_pro_1.grid(column=0, row=3, padx=(20,10)),info_person_group.lb_pro_1.config(bg="aquamarine")
        info_person_group.lb_pro_2.grid(column=0, row=4, padx=(20,10)),info_person_group.lb_pro_2.config(bg="aquamarine")

        info_person_group.tb_Id.grid(column=1, row=0, pady=5, columnspan=2, padx=(20,10))
        info_person_group.tb_name.grid(column=1, row=1,  pady=5, columnspan=2, padx=(20,10))
        info_person_group.tb_image_path.grid(column=1, row=2,  pady=5, columnspan=2, padx=(20,10))
        info_person_group.tb_pro_1.grid(column=1, row=3,  pady=5, columnspan=2, padx=(20,10))
        info_person_group.tb_pro_2.grid(column=1, row=4,  pady=5, columnspan=2, padx=(20,10))

        info_person_group.b_exit.grid(column=2, row=5, pady=15)
        info_person_group.b_save.grid(column=1, row=5, pady=15)

    def create_person_in_group(self, *argument):
        """This function creates a person and adds them to a 
        group, it can be added to any of the previous groups."""
        group_id = argument[0]
        id_1 = argument[1]
        name = argument[2]
        image_path = argument[3]
        age, gender = emotions(image_path, True)
        personId = create_person(name, image_path, group_id)
        if group_id == 1:
            pro_1 = argument[4]
            pro_2 = argument[5]
            person = Family(id_1, personId, name, gender, age, image_path, pro_1, pro_2)
            binary_files(person)
        elif group_id == 2:
            pro_1 =argument[4]
            pro_2 = argument[5]
            person = Friends(id_1, personId, name, gender, age, image_path, pro_1, pro_2)
            binary_files(person)
        elif group_id ==3:
            pro_1 = argument[4]
            pro_2 = argument[5]
            person = Famous(id_1, personId, name, gender, age, image_path, pro_1, pro_2)
            binary_files(person)
        messagebox.showinfo(message="The person has been correctly entered in the group and in the archive.", title="Message")

    def recognize(self):
        """Function that executes the information of the recognitions, but in a graphic interface with the data."""
        global recognize_window
        recognize_window = tk.Toplevel()
        recognize_window.title("Recognize person")
        recognize_window.geometry("800x200")
        recognize_window.config(bg="light blue")

        
        recognize_window.lb_inst = tk.Label(recognize_window, text="Options of groups' ID:")
        recognize_window.lb_inst.grid(column=0, row=0, padx=(20,10)),recognize_window.lb_inst.config(bg="light blue")

        recognize_window.lb_inst1 = tk.Label(recognize_window, text="1.  Family")
        recognize_window.lb_inst2 = tk.Label(recognize_window, text="2.  Friends")
        recognize_window.lb_inst3 = tk.Label(recognize_window, text="3.  Famous")
        recognize_window.lb_inst1.grid(column=0, row=1, padx=(20,10)),recognize_window.lb_inst1.config(bg="light blue")
        recognize_window.lb_inst2.grid(column=0, row=2, padx=(20,10)),recognize_window.lb_inst2.config(bg="light blue")
        recognize_window.lb_inst3.grid(column=0, row=3, padx=(20,10)),recognize_window.lb_inst3.config(bg="light blue")

        recognize_window.lb_group_id = tk.Label(recognize_window, text="Enter the group ID: ")
        recognize_window.sv_group_id = tk.StringVar()
        recognize_window.tb_group_id = tk.Entry(recognize_window, textvariable = recognize_window.sv_group_id, width=40)
        recognize_window.lb_image_path = tk.Label(recognize_window, text="Enter image's path: ")
        recognize_window.sv_image_path = tk.StringVar()
        recognize_window.tb_image_path = tk.Entry(recognize_window, textvariable = recognize_window.sv_image_path, width=40)
      
        recognize_window.lb_group_id.grid(column=1, row=2, padx=(20,10)),recognize_window.lb_group_id.config(bg="light blue")
        recognize_window.tb_group_id.grid(column=2, row=2,  pady=5, columnspan=2, padx=(20,10))
        recognize_window.lb_image_path.grid(column=1, row=3, padx=(20,10)),recognize_window.lb_image_path.config(bg="light blue")
        recognize_window.tb_image_path.grid(column=2, row=3,  pady=5, columnspan=2, padx=(20,10))
        
        recognize_window.b_exit = tk.Button(recognize_window, text = "Exit",
                                    command = lambda: self.close_window(recognize_window.destroy()))
        recognize_window.b_send = tk.Button(recognize_window, text = "Send",
                                     command = lambda: self.recognize_person(
                                     recognize_window.sv_group_id.get(),
                                     recognize_window.sv_image_path.get(),
                                     recognize_window.sv_group_id.set(""),
                                     recognize_window.sv_image_path.set("")
                                       ))                             

        recognize_window.b_exit.grid(column=3, row=7, pady=15)
        recognize_window.b_send.grid(column=2, row=7, pady=15)

    def recognize_person(self, *argument):
        """Collect all the information from the 
        person's image path in order to request the necessary data."""
        recognize_window.iconify()
        group_id = argument[0]
        image_path = argument[1]
        response = CF.face.detect(image_path)
        face_ids = [d['faceId'] for d in response]   
        identified_faces = CF.face.identify(face_ids, group_id)
        personas = identified_faces[0]
        candidates_list = personas['candidates']
        candidates = candidates_list[0]
        person = candidates['personId']
        person_data = CF.person.get(group_id, person)
        person_name = person_data['name']
        response = CF.face.detect(image_path)
        dic = response[0]
        faceRectangle = dic['faceRectangle']
        width = faceRectangle['width']
        top = faceRectangle['top']
        height = faceRectangle['height']
        left = faceRectangle['left']
        image=Image.open(image_path)
        draw = ImageDraw.Draw(image)
        draw.rectangle((left,top,left + width,top+height), outline='red', width = 7)
        font = ImageFont.truetype('C:/Users/Martin Perez/Downloads/Arial_Unicode.ttf',50)
        draw.text((50, 50), person_name, font=font, fill="black")
        image = image.resize((500,500))
        global recognize_person1
        recognize_person1 = tk.Toplevel()
        recognize_person1.img = ImageTk.PhotoImage(image)
        recognize_person1.canvas = Canvas(recognize_person1, height = 500, width = 500) 
        recognize_person1.canvas.pack(expand = YES, fill = BOTH)
        recognize_person1.canvas.create_image(0,0,  anchor = NW, image = recognize_person1.img)
        self.read_file1(group_id,person)
    
    def read_file1(self, * argument):
        """Read the binary data to request 
        registration of data on groups and individuals."""
        group_id = argument[0]
        person = argument[1]
        f = open("person.bin", "rb")
        f.seek(0)
        flag = 0
        lista= []
        while flag ==0:
            try:
                e = pickle.load(f)     
                if person==e.personId:
                    lista.extend([e.id_1, e.personId,e.name,e.gender,e.age,e.image_path])
                    if group_id=='1':
                        lista.extend([e.pro_1, e.pro_2])  
                    elif group_id=='2':
                        lista.extend([e.pro_1, e.pro_2])
                    elif group_id=='3':
                        lista.extend([e.pro_1, e.pro_2])
            except:
                print("")
                flag = 1
        f.close
        self.recognize_info_image(lista, group_id)

    def recognize_info_image(self, *argument):
        """Image recognition function based on the data requested or found by the external platform."""
        lista = argument[0]
        group_id = argument[1]
        group_id = int(group_id)
        if group_id ==1:
            text1= "The relation is:"
            text2 = "The nationality is:"
        elif group_id ==2:
            text1= "The relationship started in:"
            text2 = "The nationality is:"
        elif group_id==3:
            text1= "The profession is:"
            text2 = "The description is:"
        global data_image_window
        data_image_window = tk.Toplevel()
        data_image_window.title("Information to recognize person")
        data_image_window.geometry('450x300+700+150')
        data_image_window.config(bg="light blue")
        data_image_window.lb_iden = tk.Label(data_image_window, text="The person's ID is: ")
        data_image_window.sv_iden = tk.StringVar()
        data_image_window.tb_iden = tk.Entry(data_image_window, textvariable = data_image_window.sv_iden, width=40)
        
        data_image_window.lb_person_id = tk.Label(data_image_window, text="The person ID is: ")
        data_image_window.sv_person_id = tk.StringVar()
        data_image_window.tb_person_id = tk.Entry(data_image_window, textvariable = data_image_window.sv_person_id, width=40)
        
        data_image_window.lb_name = tk.Label(data_image_window, text="The name is: ")
        data_image_window.sv_name = tk.StringVar()
        data_image_window.tb_name = tk.Entry(data_image_window, textvariable = data_image_window.sv_name, width=40)
        
        data_image_window.lb_gender = tk.Label(data_image_window, text="The gender is: ")
        data_image_window.sv_gender = tk.StringVar()
        data_image_window.tb_gender = tk.Entry(data_image_window, textvariable = data_image_window.sv_gender, width=40)

        data_image_window.lb_age = tk.Label(data_image_window, text="The age is: ")
        data_image_window.sv_age = tk.StringVar()
        data_image_window.tb_age = tk.Entry(data_image_window, textvariable = data_image_window.sv_age, width=40)

        data_image_window.lb_image_path = tk.Label(data_image_window, text="The image's path is: ")
        data_image_window.sv_image_path = tk.StringVar()
        data_image_window.tb_image_path = tk.Entry(data_image_window, textvariable = data_image_window.sv_image_path, width=40)
        
        data_image_window.lb_prop_1 = tk.Label(data_image_window, text=text1)
        data_image_window.sv_prop_1 = tk.StringVar()
        data_image_window.tb_prop_1 = tk.Entry(data_image_window, textvariable = data_image_window.sv_prop_1, width=40)

        data_image_window.lb_prop_2 = tk.Label(data_image_window, text=text2)
        data_image_window.sv_prop_2 = tk.StringVar()
        data_image_window.tb_prop_2 = tk.Entry(data_image_window, textvariable = data_image_window.sv_prop_2, width=40)

        data_image_window.lb_iden.grid(column=0, row=2, padx=(20,10)),data_image_window.lb_iden.config(bg="light blue")
        data_image_window.tb_iden.grid(column=1, row=2,  pady=5, columnspan=2, padx=(20,10))
        data_image_window.lb_person_id.grid(column=0, row=3, padx=(20,10)),data_image_window.lb_person_id.config(bg="light blue")
        data_image_window.tb_person_id.grid(column=1, row=3,  pady=5, columnspan=2, padx=(20,10))
        data_image_window.lb_name.grid(column=0, row=4, padx=(20,10)),data_image_window.lb_name.config(bg="light blue")
        data_image_window.tb_name.grid(column=1, row=4,  pady=5, columnspan=2, padx=(20,10))
        data_image_window.lb_gender.grid(column=0, row=5, padx=(20,10)),data_image_window.lb_gender.config(bg="light blue")
        data_image_window.tb_gender.grid(column=1, row=5,  pady=5, columnspan=2, padx=(20,10))
        data_image_window.lb_age.grid(column=0, row=6, padx=(20,10)),data_image_window.lb_age.config(bg="light blue")
        data_image_window.tb_age.grid(column=1, row=6,  pady=5, columnspan=2, padx=(20,10))
        data_image_window.lb_image_path.grid(column=0, row=7, padx=(20,10)),data_image_window.lb_image_path.config(bg="light blue")
        data_image_window.tb_image_path.grid(column=1, row=7,  pady=5, columnspan=2, padx=(20,10))
        data_image_window.lb_prop_1.grid(column=0, row=8, padx=(20,10)),data_image_window.lb_prop_1.config(bg="light blue")
        data_image_window.tb_prop_1.grid(column=1, row=8,  pady=5, columnspan=2, padx=(20,10))
        data_image_window.lb_prop_2.grid(column=0, row=9, padx=(20,10)),data_image_window.lb_prop_2.config(bg="light blue")
        data_image_window.tb_prop_2.grid(column=1, row=9,  pady=5, columnspan=2, padx=(20,10))
        
        data_image_window.b_exit = tk.Button(data_image_window, text = "Next",
                                    command = lambda: self.close_window(data_image_window.destroy()))
        data_image_window.b_exit.grid(column=2, row=10, pady=15)
        data_image_window.sv_iden.set(lista[0])
        data_image_window.sv_person_id.set(lista[1])
        data_image_window.sv_name.set(lista[2])
        data_image_window.sv_gender.set(lista[3])
        data_image_window.sv_age.set(lista[4])
        data_image_window.sv_image_path.set(lista[5])
        data_image_window.sv_prop_1.set(lista[6])
        data_image_window.sv_prop_2.set(lista[7])

    def create_group_window(self):
        """New group creation function with the data, but in an executable graphic interface."""
        global New_group
        New_group = tk.Toplevel()
        New_group.title("Create a new group")
        New_group.geometry("800x200+30+100")
        New_group.config(bg="pink")

        
        New_group.lb_inst = tk.Label(New_group, text="Remember that group 1, 2 and 3 already exist.")
        New_group.lb_inst.grid(column=0, row=2, padx=(20,10)),New_group.lb_inst.config(bg="pink")

        New_group.lb_group_id = tk.Label(New_group, text="Enter the group id: ")
        New_group.lb_name = tk.Label(New_group, text="Enter the name of the group: ")
        
        New_group.sv_group_id = tk.StringVar()
        New_group.tb_group_id = tk.Entry(New_group, textvariable = New_group.sv_group_id, width=40)
       
        New_group.sv_name = tk.StringVar()
        New_group.tb_name = tk.Entry(New_group, textvariable = New_group.sv_name, width=40)

        New_group.b_exit = tk.Button(New_group, text = "Exit" , 
                                     command = lambda:self.close_window(New_group.destroy()))
        New_group.b_send = tk.Button(New_group, text = "Send",
                                     command = lambda: self.create_group(
                                     New_group.sv_group_id.get(),
                                     New_group.sv_name.get(),
                                     New_group.sv_group_id.set(""),
                                     New_group.sv_name.set("")       
                                       ))
      
        New_group.lb_group_id.grid(column=1, row=2, padx=(20,10)),New_group.lb_group_id.config(bg="pink")
        New_group.tb_group_id.grid(column=2, row=2,  pady=5, columnspan=2, padx=(20,10))
        New_group.lb_name.grid(column=1, row=3, padx=(20,10)),New_group.lb_name.config(bg="pink")
        New_group.tb_name.grid(column=2, row=3,  pady=5, columnspan=2, padx=(20,10))

        New_group.b_exit.grid(column=3, row=7, pady=15)
        New_group.b_send.grid(column=2, row=7, pady=15)

    def create_group(self, *argument):
        group_id = argument[0]
        group_name = argument[1]
        CF.person_group.create(group_id, group_name)
        messagebox.showinfo(message="Group successfully created.", title="Message")

    def people_information_sorted(self):
        """Function of extraction of requested information, it is in charge of depending on the information that is requested, 2 ordering programs
        used by the teacher will be executed to be delivered of the information as it corresponds in its graphic interface."""
        global options_info_window
        options_info_window = tk.Toplevel()
        options_info_window.title("Print people information sorted by age or name")
        options_info_window.geometry("700x210")
        options_info_window.config(bg="medium turquoise")

        options_info_window.lb_inst = tk.Label(options_info_window, text="Options for the space named group_id:")
        options_info_window.lb_inst.grid(column=0, row=0, padx=(20,10)),options_info_window.lb_inst.config(bg="medium turquoise")

        options_info_window.lb_inst1 = tk.Label(options_info_window, text="1. Family")
        options_info_window.lb_inst2 = tk.Label(options_info_window, text="2. Friends")
        options_info_window.lb_inst3 = tk.Label(options_info_window, text="3. Famous")
        options_info_window.lb_inst1.grid(column=0, row=1, padx=(20,10)),options_info_window.lb_inst1.config(bg="medium turquoise")
        options_info_window.lb_inst2.grid(column=0, row=2, padx=(20,10)),options_info_window.lb_inst2.config(bg="medium turquoise")
        options_info_window.lb_inst3.grid(column=0, row=3, padx=(20,10)),options_info_window.lb_inst3.config(bg="medium turquoise")
        
        options_info_window.lb_instruc = tk.Label(options_info_window, text="Pick a request:")
        options_info_window.lb_instruc.grid(column=2, row=0, padx=(20,10)), options_info_window.lb_instruc.config(bg="medium turquoise")
        options_info_window.lb_instruc1 = tk.Label(options_info_window, text="Type 1: Person's information sorted by ages upward.")
        options_info_window.lb_instruc2 = tk.Label(options_info_window, text="Type 2: Person's information sorted by ages downward.")
        options_info_window.lb_instruc3 = tk.Label(options_info_window, text="Type 3: Person's information sorted by names upward.")
        options_info_window.lb_instruc4 = tk.Label(options_info_window, text="Type 4: Person's information sorted by names downward.")
        options_info_window.lb_instruc1.grid(column=2, row=1, padx=(20,10)), options_info_window.lb_instruc1.config(bg="medium turquoise")
        options_info_window.lb_instruc2.grid(column=2, row=2, padx=(20,10)), options_info_window.lb_instruc2.config(bg="medium turquoise")
        options_info_window.lb_instruc3.grid(column=2, row=3, padx=(20,10)), options_info_window.lb_instruc3.config(bg="medium turquoise")
        options_info_window.lb_instruc4.grid(column=2, row=4, padx=(20,10)), options_info_window.lb_instruc4.config(bg="medium turquoise")
        
        options_info_window.lb_group_id = tk.Label(options_info_window, text="Enter the group id: ")
        options_info_window.lb_case= tk.Label(options_info_window, text="Enter the request number: ")
        
        options_info_window.sv_group_id = tk.StringVar()
        options_info_window.tb_group_id = tk.Entry(options_info_window, textvariable = options_info_window.sv_group_id, width=40)
       
        options_info_window.sv_case = tk.StringVar()
        options_info_window.tb_case = tk.Entry(options_info_window, textvariable = options_info_window.sv_case, width=40)

        options_info_window.b_exit = tk.Button(options_info_window, text = "Exit" , 
                                     command = lambda:self.close_window(options_info_window.destroy()))
        options_info_window.b_send = tk.Button(options_info_window, text = "Send",
                                     command = lambda: self.sort_names_ages(
                                     options_info_window.sv_group_id.get(),
                                     options_info_window.sv_case.get(),
                                     options_info_window.sv_group_id.set(""),
                                     options_info_window.sv_case.set("")       
                                       ))
      
        options_info_window.lb_group_id.grid(column=0, row=5, padx=(20,10)),options_info_window.lb_group_id.config(bg="medium turquoise")
        options_info_window.tb_group_id.grid(column=1, row=5,  pady=5, columnspan=2, padx=(20,10))
        options_info_window.lb_case.grid(column=0, row=6, padx=(20,10)),options_info_window.lb_case.config(bg="medium turquoise")
        options_info_window.tb_case.grid(column=1, row=6,  pady=5, columnspan=2, padx=(20,10))

        options_info_window.b_exit.grid(column=3, row=7, pady=15)
        options_info_window.b_send.grid(column=1, row=7, pady=15)
    
    def sort_names_ages(self, *argument):
        """This function is in charge of gathering the information of the people 
        who are located in the groups."""
        group_id = int(argument[0])
        case = int(argument[1])
        global final_list
        if case==1:
            final_list = []
            age_upw1=ages_order(group_id)
            list2=print_people(group_id)
            for ag in age_upw1:
                for people in list2:
                    if ag==people[3]:
                        final_list.append(people)
                        list2.remove(people)
        if case==2:
            final_list = []
            age_upw2=[]
            age_upw1=ages_order(group_id)
            list2=print_people(group_id)
            for ag in age_upw1:
                for people in list2:
                    if ag==people[3]:
                        age_upw2.append(people)
                        list2.remove(people)
            for k in age_upw2[::-1]:
                final_list.append(k)
        if case==3:
            final_list = []
            names_upw1=names_sorted(group_id)
            list2=print_people(group_id)
            for nm in names_upw1:
                for people in list2:
                    if nm==people[2]:
                        final_list.append(people)
                        list2.remove(people)
        if case==4:
            final_list = []
            names_upw2=[]
            names_upw1=names_sorted(group_id)
            list2=print_people(group_id)
            for nm in names_upw1:
                for people in list2:
                    if nm==people[2]:
                        names_upw2.append(people)
                        list2.remove(people)
            for k in names_upw2[::-1]:
                final_list.append(k)
        options_info_window.iconify()
        self.sorted_info_age_name(group_id)
        
    def sorted_info_age_name(self, *argument):
        """Data window creation function for the execution of the selected group with the data of that group. """
        group_id = int(argument[0])
        if group_id ==1:
            text1= "The relation is:"
            text2 = "The nationality is:"
        elif group_id ==2:
            text1= "The relationship started in:"
            text2 = "The nationality is:"
        elif group_id==3:
            text1= "The profession is:"
            text2 = "The description is:"
        global sorted_info
        sorted_info = tk.Toplevel()
        sorted_info.title("Info recognize person")
        sorted_info.geometry('450x300+700+150')
        sorted_info.config(bg="light blue")
        sorted_info.lb_iden = tk.Label(sorted_info, text="The person's ID is: ")
        sorted_info.sv_iden = tk.StringVar()
        sorted_info.tb_iden = tk.Entry(sorted_info, textvariable = sorted_info.sv_iden, width=40)
        
        sorted_info.lb_person_id = tk.Label(sorted_info, text="The personID is: ")
        sorted_info.sv_person_id = tk.StringVar()
        sorted_info.tb_person_id = tk.Entry(sorted_info, textvariable = sorted_info.sv_person_id, width=40)
        
        sorted_info.lb_name = tk.Label(sorted_info, text="The name is: ")
        sorted_info.sv_name = tk.StringVar()
        sorted_info.tb_name = tk.Entry(sorted_info, textvariable = sorted_info.sv_name, width=40)
        
        sorted_info.lb_gender = tk.Label(sorted_info, text="The gender is: ")
        sorted_info.sv_gender = tk.StringVar()
        sorted_info.tb_gender = tk.Entry(sorted_info, textvariable = sorted_info.sv_gender, width=40)

        sorted_info.lb_age = tk.Label(sorted_info, text="The age is: ")
        sorted_info.sv_age = tk.StringVar()
        sorted_info.tb_age = tk.Entry(sorted_info, textvariable = sorted_info.sv_age, width=40)

        sorted_info.lb_image_path = tk.Label(sorted_info, text="The image_path is: ")
        sorted_info.sv_image_path = tk.StringVar()
        sorted_info.tb_image_path = tk.Entry(sorted_info, textvariable = sorted_info.sv_image_path, width=40)
        
        sorted_info.lb_prop_1 = tk.Label(sorted_info, text=text1)
        sorted_info.sv_prop_1 = tk.StringVar()
        sorted_info.tb_prop_1 = tk.Entry(sorted_info, textvariable = sorted_info.sv_prop_1, width=40)

        sorted_info.lb_prop_2 = tk.Label(sorted_info, text=text2)
        sorted_info.sv_prop_2 = tk.StringVar()
        sorted_info.tb_prop_2 = tk.Entry(sorted_info, textvariable = sorted_info.sv_prop_2, width=40)

        sorted_info.lb_iden.grid(column=0, row=2, padx=(20,10)),sorted_info.lb_iden.config(bg="light blue")
        sorted_info.tb_iden.grid(column=1, row=2,  pady=5, columnspan=2, padx=(20,10))
        sorted_info.lb_person_id.grid(column=0, row=3, padx=(20,10)),sorted_info.lb_person_id.config(bg="light blue")
        sorted_info.tb_person_id.grid(column=1, row=3,  pady=5, columnspan=2, padx=(20,10))
        sorted_info.lb_name.grid(column=0, row=4, padx=(20,10)),sorted_info.lb_name.config(bg="light blue")
        sorted_info.tb_name.grid(column=1, row=4,  pady=5, columnspan=2, padx=(20,10))
        sorted_info.lb_gender.grid(column=0, row=5, padx=(20,10)),sorted_info.lb_gender.config(bg="light blue")
        sorted_info.tb_gender.grid(column=1, row=5,  pady=5, columnspan=2, padx=(20,10))
        sorted_info.lb_age.grid(column=0, row=6, padx=(20,10)),sorted_info.lb_age.config(bg="light blue")
        sorted_info.tb_age.grid(column=1, row=6,  pady=5, columnspan=2, padx=(20,10))
        sorted_info.lb_image_path.grid(column=0, row=7, padx=(20,10)),sorted_info.lb_image_path.config(bg="light blue")
        sorted_info.tb_image_path.grid(column=1, row=7,  pady=5, columnspan=2, padx=(20,10))
        sorted_info.lb_prop_1.grid(column=0, row=8, padx=(20,10)),sorted_info.lb_prop_1.config(bg="light blue")
        sorted_info.tb_prop_1.grid(column=1, row=8,  pady=5, columnspan=2, padx=(20,10))
        sorted_info.lb_prop_2.grid(column=0, row=9, padx=(20,10)),sorted_info.lb_prop_2.config(bg="light blue")
        sorted_info.tb_prop_2.grid(column=1, row=9,  pady=5, columnspan=2, padx=(20,10))
        
        sorted_info.b_exit = tk.Button(sorted_info, text = "Exit",
                                    command = lambda: self.close_window(sorted_info.withdraw()))
        sorted_info.b_exit.grid(column=2, row=10, pady=15)
        
        sorted_info.b_next = tk.Button(sorted_info, text = "Next",
                                         command = lambda: self.next_register(
                                         sorted_info.sv_iden,
                                         sorted_info.sv_person_id,
                                         sorted_info.sv_name,
                                         sorted_info.sv_gender,
                                         sorted_info.sv_age,
                                         sorted_info.sv_image_path,
                                         sorted_info.sv_prop_1,
                                         sorted_info.sv_prop_2
                                         ))
        sorted_info.b_previous = tk.Button(sorted_info, text = "Previous", 
                                         command = lambda: self.previous_register(
                                         sorted_info.sv_iden,
                                         sorted_info.sv_person_id,
                                         sorted_info.sv_name,
                                         sorted_info.sv_gender,
                                         sorted_info.sv_age,
                                         sorted_info.sv_image_path,
                                         sorted_info.sv_prop_1,
                                         sorted_info.sv_prop_2
                                        ))
        sorted_info.b_next.grid(column=0, row=10, pady=15)
        sorted_info.b_previous.grid(column=1, row=10, pady=15)
        self.view_register(sorted_info.sv_iden, sorted_info.sv_person_id, sorted_info.sv_name, sorted_info.sv_gender, sorted_info.sv_age, sorted_info.sv_image_path, sorted_info.sv_prop_1,sorted_info.sv_prop_2)

    def view_register(self, sv_iden, sv_person_id, sv_name, sv_gender, sv_age, sv_image_path, sv_prop_1, sv_prop_2):
        """Function that shows in window the information for the user that you have selected first."""
        global final_list
        global current_register
        self.sv_iden = sv_iden
        self.sv_person_id = sv_person_id
        self.sv_name = sv_name
        self.sv_gender = sv_gender
        self.sv_age = sv_age
        self.sv_image_path = sv_image_path
        self.sv_prop_1 = sv_prop_1
        self.sv_prop_2 = sv_prop_2
        self.person = final_list[current_register]
        self.sv_iden.set(self.person[0])
        self.sv_person_id.set(self.person[1])
        self.sv_name.set(self.person[2])
        self.sv_gender.set(self.person[3])
        self.sv_age.set(self.person[4])
        self.sv_image_path.set(self.person[5])
        self.sv_prop_1.set(self.person[6])
        self.sv_prop_2.set(self.person[7])

    def next_register(self, sv_iden, sv_person_id, sv_name, sv_gender, sv_age, sv_image_path, sv_prop_1, sv_prop_2):
        """Function that displays in window the user information of the next person you want to see."""
        global current_register
        global final_list
        if current_register < len(final_list)-1:
            current_register += 1
            self.view_register(sv_iden, sv_person_id, sv_name, sv_gender, sv_age, sv_image_path, sv_prop_1, sv_prop_2)

    def previous_register(self, sv_iden, sv_person_id, sv_name, sv_gender, sv_age, sv_image_path, sv_prop_1, sv_prop_2):
        """Function that shows in window the user information of the previous person you want to see."""
        global current_register
        if current_register > 0:
            current_register -=1
            self.view_register(sv_iden, sv_person_id, sv_name, sv_gender, sv_age, sv_image_path, sv_prop_1, sv_prop_2)

if __name__ =="__main__":
    root = tk.Tk()
    root.geometry("975x400")
    current_register = 0
    final_list = []
    main = Window(root)
    main.img = Image.open("C:/Users/Martin Perez/Downloads/1.png")
    main.img = main.img.resize((1000, 400), Image.ANTIALIAS)
    main.img = ImageTk.PhotoImage(main.img)
    main.canvas = Canvas(main, width=1000, height=400)
    main.canvas.pack(fill="both")
    main.canvas.create_image(0,0, anchor=NW, image= main.img)
root.mainloop()
#C:/Users/Martin Perez/Downloads/t.jpg
